'use strict';

var utils = require('../utils/writer.js');
var Equipment = require('../service/EquipmentService');

module.exports.activateEquipmentWithArrayInput = function activateEquipmentWithArrayInput (req, res, next) {
  var body = req.swagger.params['body'].value;
  Equipment.activateEquipmentWithArrayInput(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.createEquipment = function createEquipment (req, res, next) {
  var vesselCode = req.swagger.params['vesselCode'].value;
  var body = req.swagger.params['body'].value;
  Equipment.createEquipment(vesselCode,body)
    .then(function (response) {
      utils.writeJson(res, response, 201);
    })
    .catch(function (response) {
      if(response == "duplicated"){
        // res.status(409);
        utils.writeJson(res, response, 409);
      }
      else{
        utils.writeJson(res, response);
      }
  });
};

module.exports.desactivateEquipmentWithArrayInput = function desactivateEquipmentWithArrayInput (req, res, next) {
  var body = req.swagger.params['body'].value;
  Equipment.desactivateEquipmentWithArrayInput(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getEquipmentsByVessel = function getEquipmentsByVessel (req, res, next) {
  var vesselCode = req.swagger.params['vesselCode'].value;
  Equipment.getEquipmentsByVessel(vesselCode)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
