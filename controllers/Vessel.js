'use strict';

var utils = require('../utils/writer.js');
var Vessel = require('../service/VesselService');

module.exports.addVessel = function addVessel (req, res, next) {
  var body = req.swagger.params['body'].value;
  Vessel.addVessel(body)
    .then(function (response) {
      utils.writeJson(res, response, 201);
    })
    .catch(function (response) {
      if(response == "duplicated"){
        utils.writeJson(res, response, 409);
      }
      else{
        utils.writeJson(res, response, 500);
      }
    });
};
