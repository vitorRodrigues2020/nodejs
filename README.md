
### Running the server
To run the server, run:

```
docker-compose up -d
npm i
npm start
```

To view the Swagger UI interface:

```
open http://localhost:8080/docs
```

Application running on 
http://localhost:8080


You can use swagger interface to run access the api.

### Running tests
```
npm test
```