//Import the mongoose module
var mongodb = require('./mongodb');

module.exports = {add, desactivate, getAll}

function add(key, data, next){
    var db = new database();
    db.insert(key, data, (err) => {
        next(err);
    });
}

function getAll(key, data, next){
    var db = new database();
    db.getAll(key, {vessel: data, status: true}, (err, data) => {
        next(err, data);
    });
}

function desactivate(key, data, next){
    var db = new database();
    db.desactivate(key, data, (err) => {
        next(err);
    });
}

function database(){
    this.insert = function(key, data, next){
        mongodb(key, data, "insert", (err) => {
            next(err)
        });        
    }

    this.getAll = function(key, data, next){
        mongodb(key, data, "getAll", (err, data) => {
            next(err, data)
        });
    }

    this.desactivate = function(key, data, next){
        mongodb(key, data, "desactivate", (err) => {
            next(err)
        });
    }
}

