//Import the mongoose module
var mongoose = require('mongoose');

//Set up default mongoose connection
var mongoDB = 'mongodb://root:secret@localhost:27017/admin';

var models = [];

module.exports = function(key, data, operation, next) {
    mongoose.connect(mongoDB);

    //Get the default connection
    var db = mongoose.connection;

    db.on('error', function(){
        console.log("could not connect")
    });
    
    db.once('open', function() {
        console.log("Connection Successful!");
        
        var model = models.filter(x => x.key == key);        

        if(key == "vessel"){
            if(model == undefined || model == null || model.length == 0){
                // define Schema
                var vessel = mongoose.Schema({
                    code: String,
                    status: Boolean
                    });

                // compile schema to model
                model = mongoose.model('vessel', vessel, 'vessel');
                models.push({key: "vessel", model: model});
            }            
            else{
                model = model[0].model;
            }

            if(operation == "insert"){
                var modelInsert = new model(data);

                model.findOne({code: data.code},(err, result) => {
                    if(err){
                        mongoose.disconnect();

                        next(err);
                    }
                    else if(result){
                        mongoose.disconnect();

                        next("duplicated");
                    }
                    else{
                        modelInsert.save(function (err, book) {
                            if (err){
                                mongoose.disconnect();

                                next(err);
                            } 
                            else{
                                mongoose.disconnect();

                                next(null);
                            }
                        });
                    }                    
                });
            }
        }
        else if(key == "equipments"){
            if(model == undefined || model == null || model.length == 0){
                // define Schema
                var equipment = mongoose.Schema({
                    code: String,
                    name: String,
                    location: String,
                    vessel: String,
                    status: Boolean
                });

                // compile schema to model
                model = mongoose.model('equipment', equipment, 'equipment');
                models.push({key: "equipments", model: model});
            }
            else{
                model = model[0].model;
            }
            

            if(operation == "insert"){
                var modelInsert = new model(data);

                model.findOne({code: data.code},(err, result) => {
                    if(err){
                        // db.close();
                        next(err.message);
                        mongoose.disconnect();
                    }
                    else if(result){
                        // db.close();
                        mongoose.disconnect();
                        next("duplicated");
                    }
                    else{
                        modelInsert.save(function (err, book) {
                            if (err){
                                // db.close();
                                next(err);
                            } 
                            else{
                                next(null)
                            }
                            mongoose.disconnect();

                        });
                    }                    
                });
            }
            else if(operation == "getAll"){
                model.find(data,(err, result) => {
                    if(err){
                        // db.close();
                        mongoose.disconnect();
                        next(err.message);
                    }
                    else{
                        mongoose.disconnect();
                        next(null, result);
                    }               
                });
            }
            else if(operation == "desactivate"){
                for(var i=0; i<data.length; i++){
                    model.update({code: data[i].code}, {status: false}, (err) => {
                        if(err){
                            mongoose.disconnect();
                            next(err);
                        }
                        else if(i == data.length){
                            mongoose.disconnect();

                            next(null);
                        }
                    });
                }
            }
            else{
                mongoose.disconnect();

                next("could not fould function for key "+ key);
            }
        }
        else{
            mongoose.disconnect();

            next("Could not found Key")
        }
    });    
}
