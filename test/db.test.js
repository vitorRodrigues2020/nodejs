var expect  = require('chai').expect;
let chaiHttp = require('chai-http');
var chai = require('chai')
var equipentsApi = require('../utils/db');

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

describe('APis', function() {
    this.timeout(15000);
    var vessel = makeid(5);
    var equipment = makeid(7);

   it("test api", (done) => {
       equipentsApi.add("vessel",{code: vessel, status:true}, (err) => {
           expect(err).equal(null);
           done();
       });
    });

    it("vessel duplicated", (done) => {
        equipentsApi.add("vessel",{code: vessel, status:true}, (err) => {
            expect(err).equal("duplicated");
            done();
        });
     });

     it("add equipments", (done) => {
        var body = {
            "code": equipment,
            "vessel": vessel,
            "name": "test mock",
            "location": "mock",
            "status": true
          }
        equipentsApi.add("equipments", body, (err) => {
            expect(err).equal(null);
            done();
        });
     });
     it("equipment code duplicated", (done) => {
        var body = {
            "code": equipment,
            "vessel": vessel,
            "name": "test mock",
            "location": "mock",
            "status": true
          }
        equipentsApi.add("equipments", body, (err) => {
            expect(err).equal("duplicated");
            done();
        });
     });
     it("get equipment", (done) => {
        equipentsApi.getAll("equipments", vessel, (err, data) => {
            expect(err).equal(null);
            expect(data).to.not.equal(null);
            done();
        });
     });
     it("disactivated equipment", (done) => {
        equipentsApi.desactivate("equipments", [{code: equipment}], (err) => {
            expect(err).equal(null);
            done();
        });
     });

     it("get equipment return null", (done) => {
        equipentsApi.getAll("equipments", vessel, (err, data) => {
            expect(err).equal(null);
            expect(data.length).equal(0);
            done();
        });
     });
});