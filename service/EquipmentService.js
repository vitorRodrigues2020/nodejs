'use strict';
var db = require('../utils/db');


/**
 * Activate a list of equipments
 * 
 *
 * body List List of equipment object
 * no response value expected for this operation
 **/
exports.activateEquipmentWithArrayInput = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Create a new equipment
 * Create a new equipment with unique code.
 *
 * vesselCode String ID of vessel
 * body Equipment Created equipment to vessel
 * no response value expected for this operation
 **/
exports.createEquipment = function(vesselCode,body) {
  return new Promise(function(resolve, reject) {
    db.add("equipments", body, (err) => {
      if(err){
        reject(err);
      }
      else{
        resolve();
      }
    })
  });
}


/**
 * Desactivate a list of equipments
 * 
 *
 * body List List of equipment object
 * no response value expected for this operation
 **/
exports.desactivateEquipmentWithArrayInput = function(body) {
  return new Promise(function(resolve, reject) {
    db.desactivate("equipments", body, (err) => {
      if(err){
        reject(err);
      }
      else{
        resolve();
      }
    })
  });
}


/**
 * Find equipements by vessel
 * Returns all equipments in a vessel
 *
 * vesselCode String ID of vessel to return
 * returns List
 **/
exports.getEquipmentsByVessel = function(vesselCode) {
  return new Promise(function(resolve, reject) {
    db.getAll("equipments", vesselCode, (err, data) => {
      if(err){
        reject(err);
      }
      else{
        var examples = {};
        examples['application/json']  = data
        resolve(examples[Object.keys(examples)[0]])
      }
    });
  });
}

