'use strict';
var db = require('../utils/db');


/**
 * Add a new vessel
 * Add a new vessel with unique code
 *
 * body Vessel Vessel object should be added
 * no response value expected for this operation
 **/
exports.addVessel = function(body) {
  return new Promise(function(resolve, reject) {
    db.add("vessel", body, (err) => {
      if(err){
        reject(err);
      }
      else{
        resolve("Success");
      }
    });    
  });
}

